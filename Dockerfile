FROM python:3.9.13-slim-bullseye
WORKDIR /Avenger/
RUN apt-get update && apt-get upgrade -y
RUN apt-get -qq install -y --no-install-recommends \
    wget \
    curl \
    ffmpeg \
    git \
    gnupg2 \
    imagemagick \
    apt-transport-https \
    libjpeg-turbo-progs \
    libpng-dev \
    libwebp-dev
RUN python3.9 -m pip install -U pip
RUN apt-get install -y python3-pip
RUN pip3 install wheel
COPY requirements.txt .
RUN pip3 install --no-cache-dir -U -r requirements.txt
COPY . /Avenger
CMD ["python3.9", "-m", "Avenger"]
