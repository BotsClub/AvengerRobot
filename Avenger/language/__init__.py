"""Avenger language"""

from typing import AsyncIterator

from aiopath import AsyncPath


async def get_lang_file() -> AsyncIterator[AsyncPath]:
    async for language_file in AsyncPath("Avenger/language").iterdir():
        if language_file.suffix == ".yml":
            yield language_file
