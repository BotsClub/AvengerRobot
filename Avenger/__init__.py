"""Avenger init"""

__version__ = "2.3"

__description__ = (
    "Telegram group management bot with spam protection powered by Machine Learning and tesseract."
)

DEFAULT_CONFIG_PATH = "config.env"
