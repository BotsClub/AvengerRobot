"""Avenger database core"""

import sys
from typing import TYPE_CHECKING, Any

from Avenger import util

from .Avenger_mixin_base import MixinBase

if TYPE_CHECKING:
    from .Avenger_bot import Avenger


class DatabaseProvider(MixinBase):
    db: util.db.AsyncDatabase

    def __init__(self: "Avenger", **kwargs: Any) -> None:
        if sys.platform == "win32":
            import certifi

            client = util.db.AsyncClient(
                self.config["db_uri"], connect=False, tlsCAFile=certifi.where()
            )
        else:
            client = util.db.AsyncClient(self.config["db_uri"], connect=False)

        self.db = client.get_database("AvengerBot")

        # Propagate initialization to other mixins
        super().__init__(**kwargs)
