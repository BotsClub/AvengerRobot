"""Avenger mixin base"""

from typing import TYPE_CHECKING, Any

MixinBase: Any
if TYPE_CHECKING:
    from .Avenger_bot import Avenger

    MixinBase = Avenger
else:
    import abc

    MixinBase = abc.ABC
