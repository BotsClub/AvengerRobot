"""Avenger custom types"""

from abc import abstractmethod, abstractproperty
from typing import TYPE_CHECKING, Any, Iterable, Protocol, TypeVar

from pyrogram.filters import Filter

if TYPE_CHECKING:
    from Avenger.core import Avenger

Bot = TypeVar("Bot", bound="Avenger", covariant=True)
ChatId = TypeVar("ChatId", int, None, covariant=True)
TextName = TypeVar("TextName", bound=str, covariant=True)
NoFormat = TypeVar("NoFormat", bound=bool, covariant=True)
TypeData = TypeVar("TypeData", covariant=True)


class CustomFilter(Filter):  # skipcq: PYL-W0223
    anjani: "Avenger"
    include_bot: bool


class NDArray(Protocol[TypeData]):
    @abstractmethod
    def __getitem__(self, key: int) -> Any:
        raise NotImplementedError

    @abstractproperty
    def size(self) -> int:
        raise NotImplementedError


class Pipeline(Protocol):
    @abstractmethod
    def predict(self, X: Iterable[Any], **predict_params: Any) -> NDArray[Any]:
        raise NotImplementedError

    @abstractmethod
    def predict_proba(self, X: Iterable[Any]) -> NDArray[Any]:
        raise NotImplementedError
