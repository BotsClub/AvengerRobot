"""Avenger base chat action"""

import asyncio
from types import TracebackType
from typing import TYPE_CHECKING, Optional, Type

from pyrogram.enums.chat_action import ChatAction
from pyrogram.errors import FloodWait
from pyrogram.types import Chat

if TYPE_CHECKING:
    from Avenger.command import Context
    from Avenger.core import Avenger


class BotAction:

    # Instances variable
    __running: bool
    __current: ChatAction
    __chat: Chat

    bot: "Avenger"
    loop: asyncio.AbstractEventLoop

    # Instance variable to be filled later
    __task: asyncio.Task[None]

    def __init__(self, ctx: "Context", action: ChatAction = ChatAction.TYPING) -> None:
        self.__running = True
        self.__current = action
        self.__chat = ctx.chat

        self.bot = ctx.bot
        self.loop = ctx.bot.loop

    async def __cancel(self) -> None:
        try:
            await self.bot.client.send_chat_action(self.__chat.id, ChatAction.CANCEL)
        except FloodWait as e:
            await asyncio.sleep(e.value)  # type: ignore

    async def __start(self) -> None:
        while self.__running:
            try:
                await self.bot.client.send_chat_action(self.__chat.id, self.__current)
            except FloodWait as e:
                await asyncio.sleep(e.value)  # type: ignore
            else:
                await asyncio.sleep(1)

    async def __stop(self) -> None:
        self.__running = False
        await self.__cancel()

        if not self.__task.done():
            self.__task.cancel()
        else:
            self.__task.result()

    async def switch(self, action: ChatAction) -> None:
        """Switch current BotAction"""
        # avoid race condition with current action
        async with asyncio.Lock():
            await self.__cancel()
            self.__current = action

    def __enter__(self) -> "BotAction":
        self.__task = self.loop.create_task(self.__start())
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[Exception]],
        exc: Optional[Exception],
        tb: Optional[TracebackType],
    ) -> None:
        self.loop.create_task(self.__stop())

    async def __aenter__(self) -> "BotAction":
        return self.__enter__()

    async def __aexit__(
        self,
        exc_type: Optional[Type[Exception]],
        exc: Optional[Exception],
        tb: Optional[TracebackType],
    ) -> None:
        await self.__stop()
